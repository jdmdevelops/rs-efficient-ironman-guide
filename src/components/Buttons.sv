<script>
  import { currentMilestone, milestone } from "./stores.js";
  import data from "./data";

  $: currentStep = $milestone[$currentMilestone];

  function handleKeypress(e) {
    if (e.key == "ArrowLeft") {
      decrement();
    }
    if (e.key == "ArrowRight") {
      increment();
    }
  }

  let milestoneKeys = Object.keys(data);

  function decrement() {
    if (currentStep > 0) $milestone[$currentMilestone]--;

    if (currentStep == 0) {
      if (milestoneKeys.indexOf($currentMilestone) > 0)
        $currentMilestone =
          milestoneKeys[milestoneKeys.indexOf($currentMilestone) - 1];
    }
  }

  function increment() {
    if (currentStep < data[$currentMilestone].length - 1) {
      $milestone[$currentMilestone]++;
    }
    if (currentStep == data[$currentMilestone].length - 1) {
      if (milestoneKeys.indexOf($currentMilestone) + 1 < milestoneKeys.length)
        $currentMilestone =
          milestoneKeys[milestoneKeys.indexOf($currentMilestone) + 1];
    }
  }
</script>

<style>
  .container {
    display: flex;
    justify-content: center;
  }
  .button {
    font-size: 20px;
    width: 150px;
    height: 50px;
    text-align: center;
    padding-top: 10px;
    border-radius: 5px;
    margin: 15px;
    margin-top: 0px;
    margin-right: 0px;
    box-shadow: 2px 1px 3px rgba(0, 0, 0, 0.2);
  }
  .previous {
    background-color: #db3236;
  }
  .next {
    background-color: #3cba54;
  }
</style>

<div class="container">
  <div class="button previous" on:click={decrement}>Previous</div>
  <div class="button next" on:click={increment}>Next</div>
</div>

<svelte:window on:keyup={handleKeypress} />

<script>
  export let title = "Title";
  export let step = 0;
  export let details = "Details";
</script>

<style>
  .header {
    display: flex;
    flex-direction: column;
  }
  .title {
    align-self: center;
    font-size: 30px;
    font-weight: bold;
  }
  .container {
    display: flex;
    justify-content: center;
    font-size: 15px;
  }
  .step {
    padding-right: 10px;
  }
</style>

<div class="header">
  <div class="title">{title}</div>
  <div class="container">
    {#if step > 0}
      <div class="step">Step {step}</div>
    {/if}
  </div>
</div>

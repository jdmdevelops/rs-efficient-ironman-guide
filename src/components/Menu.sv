<script>
  import data from "./data.js";
  import { currentMilestone } from "./stores.js";

  let steps = Object.values(data);
  let milestones = steps.map(step => step[0]);
  let collapsed = false;
  let width = window.innerWidth;

  function resize() {
    width = window.innerWidth;
  }

  function collapse() {
    collapsed = !collapsed;
  }

  function changeMilestone(milestone) {
    $currentMilestone = milestone.title;
  }
</script>

<style>
  .milestone:hover {
    background-color: #eee;
    cursor: pointer;
  }
  .title {
    font-size: 20px;
    font-weight: bold;
    text-align: center;
  }

  hr {
    border: 1px solid rgba(0, 0, 0, 0.1);
  }

  @media (max-width: 760px) {
    /* Mobile */
    .index {
      font-size: 20px;
      font-weight: bold;
      text-align: center;
    }
    .index:hover {
      background-color: #eee;
      cursor: pointer;
    }

    .container {
      padding: 10px;
      box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    }

    .goals {
      display: flex;
      justify-content: center;
      font-size: 15px;
      margin: 5px;
      padding: 0px;
    }
  }

  @media (min-width: 760px) {
    /* Desktop */
    .milestone {
      padding: 10px;
    }
    .index {
      display: none;
    }
    .container {
      width: 200px;
      height: 100vh;
      box-shadow: 2px 0px 10px rgba(0, 0, 0, 0.3);
    }
    .goals {
      margin: 5px;
      padding-left: 20px;
    }
  }
</style>

<svelte:window on:resize={resize} />

<div class="container">
  <div class="index" on:click={collapse}>Index</div>
  {#if collapsed || width > 760}
    {#each milestones as milestone}
      <div class="milestone" on:click={() => changeMilestone(milestone)}>
        <div class="title">{milestone.title}</div>
        <hr />
        {#each milestone.goals as goal}
          <ul class="goals">
            <li>{goal}</li>
          </ul>
        {/each}
      </div>
    {/each}
  {/if}
</div>

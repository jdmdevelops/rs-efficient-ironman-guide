<script>
  import Header from "./Header";
  import Content from "./Content";
  import Buttons from "./Buttons";
  import Card from "./Card";
  import Menu from "./Menu";
  import data from "./data.js";
  import { currentMilestone, milestone } from "./stores.js";

  $currentMilestone = localStorage.getItem("milestone")
    ? localStorage.getItem("milestone")
    : $currentMilestone;

  $milestone[$currentMilestone] = localStorage.getItem("step")
    ? localStorage.getItem("step")
    : $milestone[$currentMilestone];
  $: step = $milestone[$currentMilestone];

  $: localStorage.setItem("milestone", $currentMilestone);

  $: localStorage.setItem("step", step);

  $: stepData = data[$currentMilestone][step];

  $: title = stepData.title ? stepData.title : stepData.location;
</script>

<style>
  :global(b) {
    font-weight: bold;
  }

  * {
    font-family: "Roboto", sans-serif;
  }
  hr {
    border: 1px solid rgba(0, 0, 0, 0.1);
  }

  .app {
    display: flex;
  }

  @media (max-width: 760px) {
    .app {
      flex-direction: column;
    }
  }
</style>

<div class="app">
  <Menu />

  <Card>
    <Header {title} {step} />
    <hr />
    <Content {stepData} />
    <Buttons />
  </Card>
</div>

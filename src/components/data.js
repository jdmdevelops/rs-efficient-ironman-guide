export default {
    "1.1": [
        // {
        //     location: "",
        //     itemsRequired: [
        //         "",
        //     ],
        //     steps: [
        //         {
        //             title: "",
        //             steps: [
        //                 "",
        //             ]
        //         },
        //         {
        //             steps: [
        //                 "",
        //             ]
        //         }
        //     ]
        // },
        // {
        //     title: "",
        //     goals: [""]
        // },
        {
            title: "1.1",
            goals: ["Early quests", "Wintertodt", "Ardougne cloak 1"]
        },
        {
            location: "Taverly",
            itemsRequired: [
                "GP, cheese, runes, all 4 meat for druidic ritual, 18 cakes, equip gloves"
            ],
            steps: [
                {
                    title: "Do Witch's house",
                    steps: [
                        "Tank the first 2 phases on the north side of the shed and run to safespot when you need to eat",
                        "It's possible to safespot the first 2, but with fire strike you are in no real danger of dying with cakes."
                    ]
                },
                {
                    steps: [
                        "Do Druidic ritual",
                        "Buy 1 PACK of eyes of newt and pestle and mortar in Taverly"
                    ]
                }
            ]
        },
        {
            location: "Port Sarim",
            itemsRequired: [
                "Pickaxe, eye of newt, 2 iron bars, bucket of water, pie dish, redberries, some food"
            ],
            steps: [
                {
                    title: "Make redberry pie",
                    steps: [
                        "You can buy flour and redberries at port sarim food shop."
                    ]
                },
                {
                    steps: [
                        "From the food shop, buy 1 raw meat also and burn it (cook twice)"
                    ]
                }
            ]
        },
        {
            location: "Rimmington",
            steps: [
                {
                    steps: ["Do Witch's potion quest"]
                },
                {
                    title: "Finish the Knight's sword quest",
                    steps: ["Mine 2 blurites, 1 is for diary"]
                }
            ]
        },
        {
            location: "Al Kharid",
            steps: [
                {
                    steps: [
                        "Home tele and go to al kharid",
                        "Buy 50 thread, 3 needles and all of the moulds from the crafting shop.",
                        "Start prince ali rescue and talk to Osman"
                    ]
                }
            ]
        },
        {
            location: "Desert",
            steps: [
                {
                    steps: [
                        "Buy 1 inv of bronze bars for questing from shantay pass."
                    ]
                },
                {
                    title: "Do the tourist trap",
                    steps: [
                        "Safespot for the captain <a href='https://www.youtube.com/watch?v=VFxB4zWqrd8'>video</a>",
                        "<b> PUT BOTH XP REWARDS ON AGILITY, HOLD SPACE SO YOU DON'T ACCIDENTALLY PUT ONE ON THIEVING </b>"
                    ]
                }
            ]
        },
        {
            location: "Port Khazard",
            steps: [
                {
                    steps: [
                        "Minigame tele to fish trawler",
                        "Buy 2 buckets of slime",
                        "Buy 10 flour from general store"
                    ]
                },
                {
                    title:
                        "Buy buckets of sand and soda ash until you hit the log-in limit.",
                    steps: [
                        "You only need to buy around 1200 buckets of sand and soda ash in total, rest of crafting will be done with other methods. <b>I recommend buying these supplies when you're gonna go afk for more than 10 mins, this way you can buy them again when you log in</b>"
                    ]
                },
                {
                    steps: ["Mine 15 clay south of Khazard"]
                }
            ]
        },
        {
            location: "South of Ardougne",
            itemsRequired: ["Fire strike, barcrawl card, cakes, axe"],
            steps: [
                {
                    steps: [
                        "Fire strike imps west of the tower of life for all 4 beads",
                        "Do Fight arena"
                    ]
                },
                {
                    title:
                        "Do Tree gnome village, get Glarial's pebble on the way for Waterfall",
                    steps: [
                        "Safespot the khazard warlord, either by using another account to block him or just by walking in and out of his aggro range."
                    ]
                }
            ]
        },
        {
            location: "Battlefield of Khazard",
            steps: [
                {
                    title:
                        "Run to ZMI bank and safespot the zamorak warrior until you get a rune scimitar",
                    steps: [
                        "Getting the scim is optional but it's not too rare and you're still getting mage xp.",
                        "You can also buy a rune sword from champion's guild if you don't want to camp for the scimitar."
                    ]
                },
                {
                    steps: ["Get a drink from Ardy bar for the barcrawl."]
                }
            ]
        },
        {
            location: "Ardougne",
            steps: [
                {
                    steps: ["Buy 4 compost pack from the Ardougne farming shop"]
                },
                {
                    title: "Train 42 magic at Moss giants near fishing guild.",
                    steps: [
                        "Bury the bones",
                        " From this point on, use teleports if it saves time/run energy. I'm not going to write every teleport to this guide."
                    ]
                }
            ]
        },
        {
            location: "Ardougne",
            steps: [
                {
                    steps: [
                        "Do Hazeel cult",
                        "Get a drink for the barcrawl quest from brimhaven bar"
                    ]
                },
                {
                    title: "Do Tribal totem",
                    steps: [
                        " Buy some swordfish and karambwans from the food shop where you start the quest at for some of the more difficult quests.",
                        "<b>Inventory:</b> No GP, glarial's pebble, notes for dwarf cannon and ammo mould, rope, 6 air/water/earth runes, cakes"
                    ]
                },
                {
                    steps: [
                        "Finish Dwarf Cannon",
                        "Drop runes outside glarial's tomb, get amulet and the urn",
                        "Finish Waterfall Quest"
                    ]
                }
            ]
        },
        {
            location: "Port Khazard",
            steps: [
                {
                    steps: [
                        "Minigame tele to fish trawler",
                        "Charter to port sarim",
                        "go to zeah and get 15% hosidius favour, 100 compost and saltpetre"
                    ]
                },
                {
                    title:
                        "Train 42 thieving at the fruit stall near farming patch, bank strange fruit and golovanova tops",
                    steps: [
                        "You can trap the dogs inside the building",
                        "Use strange fruits for energy restoration, they won't last long but you can do a few quests with them"
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            steps: [
                {
                    steps: ["Home tele to Lumbridge"]
                },
                {
                    steps: ["Do cook's assistant"]
                },
                {
                    steps: [
                        "Start RFD and watch the cutscene to unlock the bank chest"
                    ]
                },
                {
                    steps: ["Buy 7 buckets of milk"]
                }
            ]
        },
        {
            location: "Lumbridge",
            itemsRequired: ["No GP, 20 balls of wool, 8 cakes"],
            steps: [
                {
                    steps: ["Do sheep shearer"]
                },
                {
                    title: "Pickpocket HAM members for a rusty sword",
                    steps: ["If you get them, keep robes, opal and buttons"]
                },
                {
                    steps: ["Pick up 12 onion at Fred's farm"]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: [
                        "Get the key imprint, wig and skin paste for Prince ali rescue",
                        "Start Vampire slayer, get 3 garlic upstairs from the cupboard"
                    ]
                },
                {
                    title: "Make 4 red, 4 blue, 5 yellow dye",
                    steps: [
                        "You can just use the ingredient on Aggie to skip all of the dialogue."
                    ]
                },
                {
                    steps: [
                        "Buy birdhouse seeds from Olivia until you hit the log-in limit",
                        "Buy 20 teleports for chronicle"
                    ]
                }
            ]
        },
        {
            location: "Wizard's tower",
            itemsRequired: [
                "<b>Equip:</b> Fire staff, chronicle",
                "<b>Inventory:</b> Fire strikes, Rune mysteries notes, 4 beads, 20 bones(kill goblins south of the draynor jail)"
            ],
            steps: [
                {
                    steps: [
                        "Finish Imp catcher",
                        "Finish Rune mysteries",
                        "Get the Ghost's skull",
                        "Kill 5 chicken/wizards, give 25 bones for demon slayer key"
                    ]
                }
            ]
        },
        {
            location: "Varrock",
            itemsRequired: [
                "GP, cadava berries and Juliet's letter(Item name is just 'message')"
            ],
            steps: [
                {
                    steps: [
                        "Chronicle tele",
                        "Continue Romeo & Juliet, get cadava potion",
                        "Buy iron chainbody from Horvik",
                        "Continue Abyss miniquest, get scrying orb, visit ess mines",
                        "Finish demon slayer",
                        "Get the stake for vampire slayer from the blue moon inn",
                        "Finish romeo and juliet",
                        "Spirit tree to gnome stronghold"
                    ]
                }
            ]
        },
        {
            location: "Gnome stronghold",
            steps: [
                {
                    steps: [
                        "Use Brimstails to go to ess mines (make sure you had the scrying orb still with you)"
                    ]
                },
                {
                    title: "Start Grand Tree",
                    steps: [
                        "<b>Inventory:</b> GP, Chronicle, Bark sample, translation notes, Barcrawl card, fire strike"
                    ]
                }
            ]
        },
        {
            location: "Clan Wars",
            steps: [
                {
                    steps: [
                        "Clan wars minigame teleport, recharge energy and go to castle wars",
                        "Get a drink for barcrawl from Yanille pub",
                        "Buy 2 bird snares, 2 butterfly jars and a butterfly net from the hunter shop"
                    ]
                }
            ]
        },
        {
            location: "Karamja",
            steps: [
                {
                    title:
                        "Go to Hazelmere and continue the grand tree until you are at Karamja shipyard",
                    steps: [
                        " From Karamja shipyard, charter to Karamja, Musa poin"
                    ]
                },
                {
                    steps: [
                        "Get a drink for barcrawl from the bar south of the banana trees"
                    ]
                }
            ]
        },
        {
            location: "Port Sarim",
            steps: [
                {
                    steps: [
                        "Get a drink from the port sarim bar for barcrawl",
                        "Do Karamja and port sarim parts of the Pirate's treasure quest"
                    ]
                }
            ]
        },
        {
            location: "Varrock",
            steps: [
                {
                    steps: [
                        "Chronicle tele",
                        "Get pirate's message from Blue moon inn, you can drop it AFTER reading the message"
                    ]
                }
            ]
        },
        {
            location: "Gnome Stronghold",
            steps: [
                {
                    title: "Finish the Grand Tree",
                    steps: [
                        "This should get you 43 mage, if you didn't get 43 then fire strike some gnome guards"
                    ]
                },
                {
                    steps: [
                        "Buy a premade blurb' sp, 4 vodka, pineapple chunks",
                        "Spirit tree to Ardougne"
                    ]
                }
            ]
        },
        {
            location: "Ardougne",
            itemsRequired: [
                "GP, Scrying orb, pickaxe, fire strike, 1 wizard mind bomb, 1 law rune"
            ],
            steps: [
                {
                    steps: [
                        "Start biohazard, bank the 3 pots and sample",
                        "Visit ess mines, scrying orb 3/3",
                        "If you didn't bank any breads when getting 25 thieving then buy 15 now from the cake stall owner",
                        "If you somehow didn't get any iron ore from wintertodt then mine 2 east of Ardy"
                    ]
                }
            ]
        },
        {
            location: "Camelot",
            steps: [
                {
                    steps: [
                        "Use mind bomb and camelot tele",
                        "Start Merlin's crystal and talk to Gawain and Lancelot"
                    ]
                }
            ]
        },
        {
            location: "Catherby",
            steps: [
                {
                    steps: [
                        "Buy candle, 2 fishing rods, lobster pot",
                        "Buy 15 pineapples from charters",
                        "Put pineapples into the compost bin",
                        "Buy 1 pack of normal compost and all farming tools, store everything in leprechaun"
                    ]
                }
            ]
        },
        {
            location: "White Wolf Mountain",
            steps: [
                {
                    steps: [
                        "Start Fishing contest",
                        "Kill Mordred and get bat bones/black candle for Merlin's crystal"
                    ]
                }
            ]
        },
        {
            location: "Seers Village",
            itemsRequired: [
                "GP, barcrawl card, pot, spade, fishing rod, garlic"
            ],
            steps: [
                {
                    steps: [
                        "Do Murder mystery",
                        "Get a drink from Seers' bar for the barcrawl"
                    ]
                },
                {
                    title: "Finish Fishing contest",
                    steps: ["<b>Inventory:</b> 6 clay, 4 copper, 2 iron"]
                }
            ]
        },
        {
            location: "Taverly/Falador",
            steps: [
                {
                    steps: [
                        "Go under the mountain and speak to Lady of the lake in Taverly",
                        "Do Doric's quest"
                    ]
                }
            ]
        },
        {
            location: "Taverly/Falador",
            steps: [
                {
                    steps: [
                        "Finish Pirate's Treasure",
                        "Make 5 molten glass",
                        "Do Black Knight's Fortress"
                    ]
                },
                {
                    title: "Do Recruitment drive",
                    steps: ["<b>Inventory: </b>GP, 4 Biohazard items, 1 bread"]
                }
            ]
        },
        {
            location: "Rimmington",
            steps: [
                {
                    steps: [
                        "Do Rimmington part of Biohazard",
                        "Buy a black wizards hat at port Sarim",
                        "Port Sarim jewellery store, give the bread to the beggar to get the excalibur",
                        "Deposit box the plague sample at Entrana monks"
                    ]
                }
            ]
        },
        {
            location: "Clan Wars",
            itemsRequired: [
                "GP, 3 planks, bronze bar, molten glass, runes, some food, 10 empty slots for antipoisons"
            ],
            steps: [
                {
                    steps: [
                        "Pick up super antipoison(1) until log in limit, decant them",
                        "Do the Observatory quest"
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            steps: [
                {
                    steps: [
                        "Home tele to lumbridge",
                        "Finish The Restless ghost",
                        "Start the Lost tribe",
                        "Teleport to Varrock"
                    ]
                }
            ]
        },
        {
            location: "Varrock",
            steps: [
                {
                    steps: [
                        "Continue Lost tribe until you need to go to the goblin village",
                        "Buy priest robes(keep these after quest), boots, gloves"
                    ]
                }
            ]
        },
        {
            location: "Edgeville",
            steps: [
                {
                    steps: [
                        "Finish the abyss miniquest",
                        "Get the magic words for Merlin's crystal at the altar",
                        "Continue Biohazard"
                    ]
                }
            ]
        },
        {
            location: "Digsite",
            steps: [
                {
                    steps: [
                        "Do the Digsite quest",
                        "Chronicle tele and start Dragon slayer at champion's guild",
                        "Use the G.E spirit tree and go to battlefield of khazard"
                    ]
                }
            ]
        },
        {
            location: "Ardougne",
            steps: [
                {
                    steps: [
                        "Finish Biohazard and may as well start underground pass while you're right there"
                    ]
                },
                {
                    title: "Finish off Ardy easy tasks, and get the cape",
                    steps: [
                        "Use lamp on agility. Not worth holding on to it, we won't be 30herb for a long time still and 2.5k xp isn't that much anyway"
                    ]
                },
                {
                    steps: ["Trade cat for 200 death runes"]
                }
            ]
        },
        {
            location: "Karamja",
            steps: [
                {
                    steps: ["Do Jungle potion"]
                },
                {
                    title: "Do Shilo village",
                    steps: [
                        " HCIM; Buy a few karambwans and swordfish from brimhaven food store. Be careful in the last multicombat dungeon, it's probably the most dangerous area for a long time. Just eat as soon as you get hit and you're fine."
                    ]
                }
            ]
        },
        {
            location: "Camelot",
            itemsRequired: [
                " Pickaxe, hammer, lit candle, excalibur, bat bones, black candle, tinderbox, knife"
            ],
            steps: [
                {
                    steps: [
                        "Teleport to Camelot",
                        "Finish Merlin's crystal",
                        "Start Holy grail and talk to Merlin"
                    ]
                }
            ]
        },
        {
            location: "Camelot",
            steps: [
                {
                    steps: ["Get boots of lightness", "Do Elemental workshop 1"]
                },
                {
                    title: "Buy 1 inv of stew from the Seers bar",
                    steps: [
                        "<b>Inventory: </b> Lost tribe brooch and book, blue/orange dyes"
                    ]
                }
            ]
        },
        {
            location: "Falador",
            steps: [
                {
                    steps: [
                        "Falador teleport",
                        "Do Goblin diplomacy",
                        "Continue Lost tribe"
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            steps: [
                {
                    steps: ["Home teleport to Lumby", "Finish Lost tribe"]
                },
                {
                    title: "Make 5 soft clay",
                    steps: ["<b>Inventory: </b>gp, 5 silver ore"]
                },
                {
                    steps: ["Go through the gate to Al Kharid"]
                }
            ]
        }
    ],
    "1.2": [
        {
            title: "1.2",
            goals: ["Theiving", "Fishing", "Mining"]
        },
        {
            location: "Desert",
            steps: [
                {
                    steps: [
                        "Start the Feud at Ali Morrisane",
                        "From Ali Morrisane, buy the desert disguise for The Feud quest, and get 15 cooking with the raw chicken that he sells",
                        "Smelt the 5 silver, make a sickle and unstrung holy symbol, keep 3 bars",
                        "Give the key imprint and bronze bar to Osman for Prince ali rescue quest"
                    ]
                }
            ]
        },
        {
            location: "Desert",
            steps: [
                {
                    title: "Do the Feud",
                    steps: [
                        "HCIM; if you're scared of going to mage bank, unlock the rune shop.",
                        "Don't bother with the blackjack miniquest, all of them have the same chance to knock out so just use the willow blackjack"
                    ]
                },
                {
                    steps: [
                        "Blackjack until 50 thieving, you can buy wines from the bar to use as food."
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            steps: [
                {
                    title:
                        "IF you're planning on 3-tick fishing then home tele to lumby and pick up 15 swamp tar now.",
                    steps: [
                        "If you think you're gonna keep messing up and making the tar multiple times, it might be worth your time getting 23 hunter and using kebbit claws with leather vambs instead. 23 hunter should take just less than 1 hour."
                    ]
                }
            ]
        },
        {
            location: "Ardougne",
            steps: [
                {
                    steps: [
                        "Ardy cloak tele and take the boat from Ardy to brimhaven",
                        "Take the cart to Shilo Village",
                        "Use the furnace to make molten glass until level 32 crafting"
                    ]
                },
                {
                    title: "Buy 25k feathers from shilo village fishing shop",
                    steps: [
                        " If you can't afford them then open some of your wintertodt crates"
                    ]
                }
            ]
        },
        {
            location: "Shilo Village",
            steps: [
                {
                    title: "Get 58 fishing with trout and salmon",
                    steps: [
                        " Bank the fish for cooking xp",
                        "You can pretty easily grow cats while doing this fishing grind since you can just feed it the fish you fish.",
                        "I recommend raising 10 cats for death runes in total, after that it's up to you if you can be bothered."
                    ]
                }
            ]
        },
        {
            location: "Burthrope",
            steps: [
                {
                    steps: [
                        "Minigame tele to Burthrope games' room",
                        "Cook all of your trout, don't cook salmon yet"
                    ]
                }
            ]
        },
        {
            location: "Barbarian Assault",
            steps: [
                {
                    title:
                        "Get 50 agility from barb fishing, should be at 74 fishing.",
                    steps: [
                        "You can feed caviar and roe to kitten, bring a knife to cut the fish",
                        " I honestly can't recommend any other way to train to 50 agi than fishing, it doesn't take that long and you'll need the fish levels eventually anyway."
                    ]
                }
            ]
        },
        {
            location: "Burthrope",
            steps: [
                {
                    steps: ["Minigame tele to Burthrope"]
                },
                {
                    title:
                        "Do the rogue's den minigame for full rogue outfit. Guide by  <a href='https://www.youtube.com/watch?v=c5tQewEZPe4'>Lelador</a>",
                    steps: [
                        " Cook the salmon to regen run energy after every time you finish 1 attempt of the minigame",
                        "Buy 50 lockpicks while you're here"
                    ]
                }
            ]
        },
        {
            location: "Pollnivneach",
            steps: [
                {
                    steps: [
                        "Equip the set and go back to blackjacking until you have 2.5m GP (around 84 thieving)"
                    ]
                },
                {
                    title: "Carpet back to Shantay pass",
                    steps: [
                        "<b>Inv:</b> teleport runes, all the golem quest items"
                    ]
                }
            ]
        },
        {
            location: "Ruins of Uzer",
            steps: [
                {
                    steps: [
                        "If you got any cosmic runes from moss giants, or unlocked Ali's rune shop then make 1 or 2 dueling rings now to speed up next 2 quests."
                    ]
                }
            ]
        },
        {
            location: "South of Al-Kharid bank",
            steps: [
                {
                    title:
                        "Do Shadow of the storm(should have priest gown and black wizard hat in bank as your black items)",
                    steps: [
                        "10K REWARD ON RANGED ALWAYS",
                        "Flinch the demon with rune scimitar, just switch to darklight for the last hit. If you didn't get r scim while training mage, go buy a rune sword from champions guild",
                        "Make an attack potion for the fight, speeds it up slightly",
                        "Use the west torch as your safespot, <a href='https://i.imgur.com/kvn6Mf5.png'>pic</a>"
                    ]
                }
            ]
        },
        {
            location: "Edgeville",
            steps: [
                {
                    steps: [
                        "Go to Edgeville and talk to Oziach to continue Dragon slayer"
                    ]
                },
                {
                    title:
                        "Go to mage bank and buy 6k nats, all of the cosmics you can get while buying nats and 10 law runes each world up to 300 laws",
                    steps: [
                        "This might take more than 1 trip. HCIM can buy them from ali morrisane if you don't wanna risk mage bank, although they're more expensive."
                    ]
                }
            ]
        },
        {
            location: "Falador",
            steps: [
                {
                    steps: [
                        "Teleport to Falador",
                        "Get 2 buckets of sap from this tree near falador east bank <a href='http://i.imgur.com/YoH5z70.png'>pic</a>",
                        "Buy addy and rune picks from the pickaxe shop in the mines."
                    ]
                }
            ]
        },
        {
            location: "Varrock",
            steps: [
                {
                    title:
                        "Complete Varrock easy diary for the armour, makes next step a bit faster",
                    steps: [
                        "You can get the kudos requirement by talking to historian Minas on the 2nd floor",
                        "Use the museum lamps on herblore",
                        "Still not 30 herb, use diary lamp on agility"
                    ]
                }
            ]
        },
        {
            location: "Ardougne",
            steps: [
                {
                    title:
                        "Mine 3000 iron ore at ardy monastery, superheat them while walking to Ardy bank and tele back with ardy cloak.",
                    steps: [
                        "3 ticking makes this a lot faster if you can be bothered",
                        "Do medium clues if you get them from mining, might get a yew bow from reward (Comp bow works too, it's for temple of ikov)"
                    ]
                },
                {
                    steps: [
                        "There are plenty of ways to get your magic up, i just like this since you get mining, smithing and later fletching xp as well.",
                        "Don't smith the bars into anything yet"
                    ]
                }
            ]
        }
    ],
    "1.3": [
        {
            title: "1.3",
            goals: ["Fairy Rings", "43 Prayer", "Kingdom", "99 Theiving"]
        },
        {
            location: "Varrock",
            steps: [
                {
                    steps: ["Smith 3 bronze wire", "Buy oak longbow"]
                }
            ]
        },
        {
            location: "Varrock",
            steps: [
                {
                    steps: [
                        "Start Priest in peril",
                        "If you didn't manage to buy 400 steel nails at the start, check sawmill now."
                    ]
                },
                {
                    title: "Do priest in peril",
                    steps: ["Start Rag and bone man on the way to the temple"]
                }
            ]
        },
        {
            location: "Mortanyia",
            steps: [
                {
                    steps: [
                        "Do Nature spirit",
                        "Buy 1 raw shark from Canifis food shop"
                    ]
                },
                {
                    title: "Do Creature of Fenkenstrain",
                    steps: [
                        "Use port Phasmatys furnace, you should have slime in bank, use it to get 5 ecto-tokens"
                    ]
                }
            ]
        },
        {
            location: "Champion's Guild",
            itemsRequired: ["Tele runes, gp, rune scim"],
            steps: [
                {
                    steps: ["Use Chronicle tele"]
                },
                {
                    title:
                        "Go champion's guild and continue Dragon slayer, ask every question",
                    steps: ["Buy a coif, green dhide chaps and vambs"]
                },
                {
                    steps: [
                        "Kill the nearby ram, bear and unicorn for their bones (Rag and bone man quest)",
                        "Camelot tele"
                    ]
                }
            ]
        },
        {
            location: "Camelot",
            steps: [
                {
                    steps: [
                        "Kill a giant bat (Rag and bone man) south west of Catherby",
                        "Do Elemental workshop 2"
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            itemsRequired: [
                "Axe, rope, lit candle, hammer, stake(vampire slayer), spade"
            ],
            steps: [
                {
                    steps: [
                        "Get anti-dragon shield from the duke",
                        "Kill a goblin for the bone for rag and bone man"
                    ]
                }
            ]
        },
        {
            location: "Lumby swamp",
            steps: [
                {
                    title: "Do Ernest the chicken, finish vampire slayer",
                    steps: [
                        "Dig up the skull for fairytale part 1 while you're in the area to save some time later"
                    ]
                },
                {
                    steps: [
                        "Get big frog leg(you need to go in to the caves), giant rat bone(both for rag and bone man)",
                        "Start Lost city",
                        "Go to Draynor manor"
                    ]
                }
            ]
        },
        {
            location: "Entrana",
            steps: [
                {
                    steps: [
                        "Talk to the high priest for Holy grail",
                        "Get at least 5 Dramen branches, make 2 staffs",
                        "Finish Lost city"
                    ]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: ["Start Fairytale part 1."]
                },
                {
                    title:
                        "The quest guide on os.rs.wiki lists where to get the 3 random items.",
                    steps: [
                        "If you get nature tally req, go to abyss with inv of food and kill leeches. Make sure you trap them so only 1 attacks"
                    ]
                },
                {
                    steps: [
                        "Flinch Tanglefoot in this spot: https://i.imgur.com/2Ejgx0h.png"
                    ]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: [
                        "Finish Fairytale part 1",
                        "Talk to martin to start Fairytale part 2",
                        "Buy 35 jugs of vinegar",
                        "Buy 1 marigold seed, 3 cabbage seeds, 3 onion seeds from Draynor seed market",
                        "Rescue prince ali from the jail"
                    ]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: [
                        "While you wait for Martin's crops for FT2",
                        "Tele to Lumby"
                    ]
                },
                {
                    title: "Start Evil Dave subquest for RFD",
                    steps: ["If you don't have a cat then raise one now."]
                },
                {
                    steps: ["Kill 2 cows and make 1 soft, 1 hard leather"]
                },
                {
                    title: "Smelt 1 inv of gold, should have from wintertodt",
                    steps: [
                        "if you didn't get any gold then go mine some from the alkharid mine",
                        "Make 1 recoil ring, 10 games necks and the rest into dueling rings",
                        "From this point just use clan wars tele as your energy recharge before teleporting to a new location"
                    ]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: [
                        "Speak to Martin for Fairytale 2",
                        "Go unlock fairy rings at Zanaris"
                    ]
                }
            ]
        },
        {
            location: "Port Sarim",
            steps: [
                {
                    steps: [
                        "Bring gp, Maze key, 90steel nails, 3 planks, hammer, food, runes",
                        "Tele to POH and do Melzar's maze (Dragon slayer quest)",
                        "Walk to port Sarim jail and telegrab 2nd map piece",
                        "Buy and repair the ship (dragon slayer)",
                        "Tele to Fally"
                    ]
                }
            ]
        },
        {
            location: "Falador",
            itemsRequired: [
                "Bring gp, runes, soft clay, wizard mind bomb, lobster pot, silk"
            ],
            steps: [
                {
                    steps: [
                        "Equip your anti-dragon shield and recoil ring now so you don't forget it later",
                        "Make unfired bowl at barb village"
                    ]
                }
            ]
        },
        {
            location: "Ice Mountain",
            steps: [
                {
                    steps: [
                        "Talk to the oracle",
                        "Get the 3rd map piece for dragon slayer",
                        "Tele to Lumby"
                    ]
                }
            ]
        },
        {
            location: "Draynor",
            steps: [
                {
                    steps: [
                        "Get Ned as your captain for dragon slayer",
                        "MAKE SURE YOU HAVE THE ANTI-DRAGON SHIELD AND RECOIL RING EQUIPPED"
                    ]
                }
            ]
        },
        {
            location: "Crandor",
            steps: [
                {
                    title:
                        "Go to Crandor and open the shortcut to Karamja volcano",
                    steps: [
                        "Bring swordfish/karambwans as your food",
                        "As soon as you go climb down from Crandor, climb back up for karamja medium diary task"
                    ]
                },
                {
                    title: "Kill Elvarg, you can do as many trips as you want",
                    steps: [
                        "There are flinch spots in the room, but honestly you shouldn't have any trouble with this fight. Step under Elvarg when eating"
                    ]
                },
                {
                    steps: [
                        "Finish dragon slayer and buy a green d'hide top from Oziach"
                    ]
                }
            ]
        },
        {
            location: "Edgeville",
            steps: [
                {
                    steps: ["Wear gloves and pick up 5 nettles at the yews"]
                },
                {
                    title:
                        "Make the stew for Evil Dave while you're at Edgeville",
                    steps: [
                        "If you don't have a cat then just leave for later, don't bother doing this with a kitten"
                    ]
                }
            ]
        },
        {
            location: "Lumbridge",
            steps: [
                {
                    steps: [
                        "Fill a bowl with water, put nettles in it and use it on a range.(This is for ghosts ahoy quest)",
                        "Finish Evil Dave subquest"
                    ]
                },
                {
                    title:
                        "Do Death to the Dorgeshuun. While getting the robes, keep buttons(If you didn't get them while getting the rusty sword)",
                    steps: ["Buy dorgeshuun crossbow and 4000 bone bolts"]
                },
                {
                    title: "Thieve Bullseye lantern from dorg chests",
                    steps: ["Buy dorgeshuun crossbow and 4000 bone bolts"]
                }
            ]
        },
        {
            location: "Camelot",
            steps: [
                {
                    steps: ["Finish Holy grail"]
                },
                {
                    title: "Do Horror from the deep quest",
                    steps: [
                        "Just mage the dagannoth and hide when it's weak to melee and range. Turn your run off so you don't get meleed, it's not difficult with blast spells. Use proper food like swordfish and karambwans."
                    ]
                }
            ]
        },
        {
            location: "Burthorpe",
            steps: [
                {
                    steps: [
                        "Do Turael slayer with dorg cbow until 37 Range",
                        "We'll be using Turael until 50 tasks completed, every 10th task should be done with the highest level slayer master you can use.",
                        "Even though we are using Turael, the goal is to get slayer xp so you should always kill monsters with higher hp, for example on bat tasks kill giant bats.Vannaka has too many bad tasks so I think this is overall better."
                    ]
                }
            ]
        },
        {
            location: "Zeah",
            steps: [
                {
                    title:
                        "Get 100% hosidius favour, should have 300 compost in bank",
                    steps: [
                        "at 45% do mess hall until 100% and don't forget to lock the favour by talking to Hosa"
                    ]
                },
                {
                    steps: [
                        "Buy mithril and rune axe from the woodcutting guild",
                        "Do Animal magnetism"
                    ]
                }
            ]
        },
        {
            location: "Varrock",
            steps: [
                {
                    steps: [
                        "Buy mithril platebody from Horvik in Varrock",
                        "Buy mithril platelegs from al-kharid"
                    ]
                },
                {
                    title: "Do Spirits of the Elid quest",
                    steps: [
                        "You can use the mithril armor for tanking the golems, or flinch them if you're taking too much damage."
                    ]
                }
            ]
        },
        {
            location: "Ardrougne",
            steps: [
                {
                    title: "Do the Underground pass quest",
                    steps: [
                        "Use the mithril armour for this quest, don't really need anything better at this point and you'll need to buy it later anyway HCIM; While this quest might seem like a dangerous one without overheads, it's honestly not. Check out the video.",
                        "Here's a video of me completing the quest with same combat stats you'd have: https://www.youtube.com/watch?v=WI2SYhpwIh8",
                        " You can trap the paladins behind each other same way as the captain from the tourist trap quest.",
                        "For the spider, there are 2 skeletons on the east wall, stand between them and the mage the big one with earth blast. Sometimes the little ones attack you but just run a bit north and then back to the same spot, they'll get stuck."
                    ]
                }
            ]
        },
        {
            location: "West Ardougne",
            steps: [
                {
                    steps: ["Upgrade the iban staff"]
                },
                {
                    title:
                        "Get 50 attack doing more Turael slayer, remember to get every 10th task from Vannaka",
                    steps: [
                        "If you get a task from Vannaka that you really can't do without melee protect then go kill sand crabs until 50 attack"
                    ]
                }
            ]
        },
        {
            location: "Falador",
            steps: [
                {
                    steps: ["Make an amulet of magic"]
                },
                {
                    title: "Kill blue dragons with iban's blast for 43 prayer",
                    steps: [
                        " We don't have the agility level for shortcut yet so don't bother banking the hides, not worth doing twice as many trips. Add 2400 xp to your current prayer xp and calculate how many bones you need, level 43 requires 50339xp and 1 bone is 288xp with ectofuntus(2.4k pray xp is from ghosts ahoy quest which will be completed before using the bones)",
                        "Regular ironmen can use the wildy altar if you want to, but either way it's only around 85 bones with ecto so do whichever method."
                    ]
                },
                {
                    steps: [
                        "Keep 1 dragon bone in your bank for the Watchtower quest"
                    ]
                }
            ]
        },
        {
            location: "Khazard",
            steps: [
                {
                    steps: [
                        "Minigame tele to khazard",
                        "Buy however many buckets of slime you need for the bones",
                        "Equip Ghostspeak amulet",
                        "If you didn't prepare the nettle water earlier, do it now"
                    ]
                }
            ]
        },
        {
            location: "Port Phasmatys",
            itemsRequired: [
                "Make sure you have all ghosts ahoy items",
                "bring GP stack, 9 d bones, 9 pot, 9 slime, general store sells pots if you don't have"
            ],
            steps: [
                {
                    steps: ["Charter to port Phasmatys"]
                }
            ]
        },
        {
            location: "",
            itemsRequired: [""],
            steps: [
                {
                    title: "",
                    steps: [""]
                },
                {
                    steps: [""]
                }
            ]
        },
        {
            location: "",
            itemsRequired: [""],
            steps: [
                {
                    title: "",
                    steps: [""]
                },
                {
                    steps: [""]
                }
            ]
        },
        {
            location: "",
            itemsRequired: [""],
            steps: [
                {
                    title: "",
                    steps: [""]
                },
                {
                    steps: [""]
                }
            ]
        },
        {
            location: "",
            itemsRequired: [""],
            steps: [
                {
                    title: "",
                    steps: [""]
                },
                {
                    steps: [""]
                }
            ]
        },
        {
            location: "",
            itemsRequired: [""],
            steps: [
                {
                    title: "",
                    steps: [""]
                },
                {
                    steps: [""]
                }
            ]
        }
    ]
}

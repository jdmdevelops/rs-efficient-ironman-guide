<style>
  @import url("https://fonts.googleapis.com/css?family=Roboto&display=swap");
  .card {
    width: 500px;
    margin: auto;
    margin-top: 25px;
    padding: 10px;
    padding-bottom: 0px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 5px;
  }
</style>

<div class="card">
  <slot>
    <!-- optional fallback -->
  </slot>
</div>

import { writable } from "svelte/store"

export const milestone = writable({
    "1.1": 0,
    "1.2": 0,
    "1.3": 0,
    "1.4": 0,
    "1.5": 0,
    "2": 0,
    "3": 0
})

export const currentMilestone = writable("1.1")

<script>
  export let stepData;
</script>

<style>
  .items {
    padding: 5px;
  }
  .content {
    font-size: 17px;
    font-weight: lighter;
  }
  .items-title {
    font-weight: bold;
  }
  .steps-title {
    font-weight: bold;
  }
  .requirements {
    margin-top: 20px;
    font-size: 17px;
    font-weight: lighter;
    padding-left: 25px;
  }
  b {
    font-weight: bold;
  }

  .goals {
    font-size: 20px;
    font-weight: lighter;
  }
  .content {
    margin-bottom: 20px;
    margin-top: 20px;
    padding-left: 25px;
  }
  .checkboxs {
    padding: 5px;
  }
</style>

{#if stepData.itemsRequired}
  <div class="requirements">
    <div class="items-title">Items Needed:</div>
    <div class="items">
      {#each stepData.itemsRequired as requirement}
        <label>
          <input type="checkbox" />
          {@html requirement}
        </label>
      {/each}
    </div>
  </div>
{/if}
<div class="content">
  {#if stepData.itemsRequired}
    <div class="steps-title">Steps:</div>
  {/if}
  {#if stepData.title}
    <ul class="goals">
      {#each stepData.goals as goal}
        <li>{goal}</li>
      {/each}
    </ul>
  {:else}
    <div class="steps">
      {#each stepData.steps as step}
        {#if step.title != null}
          <div class="checkboxs">
            <label>
              <input type="checkbox" />
              {@html step.title}
            </label>
          </div>
        {/if}

        {#each step.steps as substep}
          {#if step.title != null}
            <div class="checkboxs">
              <label>
                <input type="checkbox" />
                {@html substep}
              </label>
            </div>
          {:else}
            <div class="checkboxs">
              <label>
                <input type="checkbox" />
                {@html substep}
              </label>
            </div>
          {/if}
        {/each}
      {/each}
    </div>
  {/if}
</div>
